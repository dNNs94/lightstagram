const register = require('config/register.js');
const login = require('config/login.js');
const story = require('config/story.js');
const updateUser = require('config/updateUser.js');

module.exports = (app) => {

    app.get('/', (req, res) => {
        res.end('Nodejs-android-connection');
    });

    app.post('/login', (req, res) =>{

        const email = req.body.email;
        const password = req.body.password;

        login.login(email, password, (found) => {
            console.log(found.response);
            res.status(found.status);
            res.json(found.res);
        });
    });

    app.post('/register', (req, res) => {

        const email = req.body.email;
        const username = req.body.username;
        const password = req.body.password;

        register.register(email, username, password, (found) => {
            console.log(found.response);
            res.status(found.status);
            res.json(found.res);
        });
    });

    app.post('/logout', (req, res) => {

        const id = req.body.id;

        login.logout(id, (found) => {
            console.log(found.response);
            res.status(found.status);
            res.json(found.res);
        });
    });

    app.post('/updateUser', (req, res) => {
        const user = {
            id: req.body.id,
            username: req.body.username,
            description: req.body.description
        };

        updateUser.updateUser(user.id, user.username, user.description, (result) => {
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        })
    });

    app.post("/updateImage", (req, res) => {

        const user = {
            id: req.body.id,
            image: req.body.image
        };

        updateUser.updateUserImage(user.id, user.image, (result) => {
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        });
    });

    app.post("/search", (req, res) => {

        const keyword = req.text;

        console.log(req.text);

        updateUser.searchForUsers(keyword, (result) => {
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        });
    });

    app.post('/storyCount', (req, res) => {
        const user = {
            id: req.body.id,
            username: req.body.username,
            email: req.body.email
        };

        story.getStoryCount(user.id, (result) => {
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        })
    });

    app.post('/addStory', (req, res) => {

        const image = req.body.image,
              desc = req.body.description,
              userId = req.body.userId,
              hashtags = req.body.hashtags;

        story.addStory(userId, image, desc, hashtags, (result) => {
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        })
    });

    app.post('/story', (req, res) => {
        const user = {
            id: req.body.id,
            username: req.body.username,
            email: req.body.email,
        };

        story.getStory(user.id, (result) => {
            for(let i = 0; i < result.length; i++) {
                console.log(JSON.stringify(result[i].description, null, 2));
            }
            console.log(result.response);
            res.status(result.status);
            res.json(result.res);
        });
    });

};