// #region dependencies
const express = require('express');
const connect = require('connect');
const app = express();
const port = process.env.PORT || 8080;
// #endregion

// #region functions

/**
 * Function to handle requests of type "text/plain" and such, because bodyparser can't
 * req.text -> request body input - see routes.js line 76 app.post("/search") for usage
 * Credits: https://stackoverflow.com/questions/12497358/handling-text-plain-in-express-via-connect/12497793#12497793
 * @param req
 * @param res
 * @param next
 */
function getString(req, res, next) {
    if (req.is('text/*')) {
        req.text = '';
        req.setEncoding('utf8');
        req.on('data', function(chunk){ req.text += chunk });
        req.on('end', next);
    } else {
        next();
    }
}
// #endregion

// #region configuration
app.use(express.static(__dirname + '/public'));
app.use(connect.logger('dev'));
app.use(connect.json());
app.use(connect.urlencoded());
app.use(getString);
// #endregion

// #region routes
require('./routes/routes.js')(app);
// #endregion

app.listen(port);

console.log('Node.js Server running on port: ' + port);